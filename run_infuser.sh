#!/bin/bash
#
# run_infuser.sh
#

working_dir="/opt/compose_projects/colander/compose"

echo "$(date -Iseconds)|INFO |run_infuser.sh: started"
for pid in $(pidof -x run_infuser.sh); do
  if [ $pid != $$ ]; then
    echo "$(date -Iseconds)|INFO |run_infuser.sh: Process is already running with PID $pid"
    exit 0
  fi
done

cd "${working_dir}"
sudo /usr/bin/docker compose run infuser
echo "$(date -Iseconds)|INFO |run_infuser.sh: finished"
